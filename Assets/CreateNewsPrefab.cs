﻿using UnityEngine;
using System.Collections;

public class CreateNewsPrefab : MonoBehaviour {

	public GameObject newsColumnPrefab;//prefabNews
	public GameObject[] genRows ; //generated prefabs
	public GameObject newsColumn;

	//to be done in next part 
	public void createColumns(){
		genRows = new GameObject[20];
		Vector3 newRowsPosition= new Vector3(0,-170,0);
		//newRowsPosition = newsColumn.transform.localPosition - new Vector3 (newsColumn.transform.localPosition.x, newsColumn.transform.localPosition.y+150, newsColumn.transform.localPosition.z);
		for (int i=0; i<20; i++) {
			genRows[i]=(GameObject) Instantiate(newsColumnPrefab,newRowsPosition,newsColumn.transform.localRotation);
			genRows[i].transform.SetParent(this.transform,false);
			newRowsPosition+=new Vector3(0,-150,0);
		}
	}

	void Awake(){
		//createColumns ();
		}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
