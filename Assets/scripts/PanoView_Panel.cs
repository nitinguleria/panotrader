﻿using UnityEngine;
using System.Collections;

public class PanoView_Panel : MonoBehaviour {

	public GameObject look_at_target;
	public GameObject plane;
	private GameObject[] new_panel;

	public void createPanel()
	{
		int i;
		int k = 0;
		RaycastHit hit;
		int layerMask = 1 << 9;
		layerMask = ~layerMask;

		float duration = 4;
		foreach (GameObject j in new_panel) {
			if (j == null)
			{
				Vector3 temp= new Vector3();
				Vector3 fwd = transform.TransformDirection(Vector3.forward);
				int layerMaskSphere = 1 << 9;
				if (Physics.Raycast(transform.position, transform.forward, out hit, 1000F, layerMaskSphere))
				{
					//Debug.Log ("what did you hit?" + hit.transform.name);
					temp = hit.point;
				}
				Debug.DrawLine (transform.position, hit.point, Color.green, 1000F); //smark debugging purpose
				int number = PlayerPrefs.GetInt("currentNum"); 
				PlayerPrefs.SetInt("currentNum", number+1);

				new_panel[k] = (GameObject)Instantiate(plane, temp, transform.rotation );
				new_panel[k].transform.rotation = Quaternion.LookRotation(new_panel[k].transform.position - look_at_target.transform.position);
				
				break;
			}
			k++;
		}
		// make the new panel face the camera

	}


	public void removePanel()
	{
		RaycastHit hit;
		Vector3 temp= new Vector3();
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		int layerMask = 1 << 8;
		//layerMask = ~layerMask;

		if (Physics.Raycast(transform.position, transform.forward, out hit, 10000, layerMask) ) //9 is layermask for sphere
		{
			//Debug.Log ("Did anything got hit?"); smark
			//Debug.Log (hit.collider.name); samrk
			print ("what did you hit?");
			if (hit.collider.CompareTag("Panel"))
			{

				//Debug.Log ("Was the thing that got hit the panel?"); smark
				Destroy(hit.collider.gameObject);
			}
		}
	}


	// Use this for initialization
	void Start () 
	{
		new_panel = new GameObject[10];
		PlayerPrefs.SetInt ("boxExists", 0);
	}

	// Update is called once per frame
	void Update () 
	{

	}
}
