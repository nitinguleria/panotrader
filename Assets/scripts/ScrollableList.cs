﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScrollableList : MonoBehaviour
{

	GameObject searchInputField;
	GameObject searchResult;
	string displayNews="";

	public GameObject financialnewsScript;
     public GameObject itemPrefab; //financialnewsPrefab
     int itemCount = 21, columnCount = 1;
	//string shortDescription="";
	//int shortStringLength=2000;


    void Start()
	{   //financial news
		string displayNews="";
		FinancialNews fnews =  financialnewsScript.GetComponent<FinancialNews> ();
		ArrayList News = fnews.getNews ();
		
		ArrayList title=(ArrayList) News[0]; 
		ArrayList description=(ArrayList)News[1];

		//make prefabs= titles
		itemCount = title.Count-1;

		//assuming description length = title length -1
		for(int i=1;i<title.Count;i++)
		{
			displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";
			
		}

		//print (displayNews); 


        RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        float width = containerRectTransform.rect.width / columnCount;
        float ratio = width / rowRectTransform.rect.width;
        float height = rowRectTransform.rect.height * ratio;
        int rowCount = itemCount / columnCount;
        if (itemCount % rowCount > 0)
            rowCount++;

        //adjust the height of the container so that it will just barely fit all its children
        float scrollHeight = height * rowCount;
        containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
        containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);

        int j = 0;
        for (int i = 0; i < itemCount; i++)
        {
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
            if (i % columnCount == 0)
                j++;

            //create a new item, name it, and set the parent
            GameObject newItem = Instantiate(itemPrefab) as GameObject;
            newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
            newItem.transform.parent = gameObject.transform;

			string newsTitle=(string)title[i+1];
			string newsDescription=(string)description[i];
			//set the text to the heading and body.
			if(newsTitle.Equals("")){
				newsTitle="Yahoo! Finance: YAHOO News";
			}
			newItem.transform.FindChild("Heading").GetComponent<Text> ().text=newsTitle;

			//if(shortStringLength<= title[i+1].ToString().Length){
			//	shortDescription=((string)title[i+1]).Substring(0,shortStringLength )+"...";
			//}else
			//{
				//shortDescription=(string)title[i+1];
			//}
			if(newsDescription.Equals("")){
				newsDescription="Yahoo! Finance: YHOO News  Latest Financial News for Yahoo! Inc.";
			}

			newItem.transform.FindChild("Body").GetComponent<Text> ().text=newsDescription;
		
            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();

            float x = -containerRectTransform.rect.width / 2 + width * (i % columnCount);
            float y = containerRectTransform.rect.height / 2 - height * j;
            rectTransform.offsetMin = new Vector2(x, y);

            x = rectTransform.offsetMin.x + width;
            y = rectTransform.offsetMin.y + height;
            rectTransform.offsetMax = new Vector2(x, y);
        }
	}


	// click button for search button
	public void SearchButtonClick(string id)
	{  
		searchInputField= GameObject.Find("SearchInputField");
		searchResult = GameObject.Find ("SearchResult");
		
		
		if (id.Equals ("searchButton")) {

			//financial news
			string displayNews="";
			string searchString="";
			FinancialNews fnews =  financialnewsScript.GetComponent<FinancialNews> ();
			searchString= searchInputField.transform.GetComponentInChildren<Text> ().text;



			ArrayList News = fnews.Search (searchString);

			
			ArrayList title=(ArrayList) News[0]; 
			ArrayList description=(ArrayList)News[1];

			//make prefabs= titles
			itemCount = title.Count-1;
			
			//assuming description length = title length -1
			for(int i=1;i<title.Count;i++)
			{
				displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";
				
			}
			
			//print (displayNews); 
			
			
			//RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
			//RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();
			
			//calculate the width and height of each child item.
			//float width = containerRectTransform.rect.width / columnCount;
			//float ratio = width / rowRectTransform.rect.width;
			//float height = rowRectTransform.rect.height * ratio;
			//int rowCount = itemCount / columnCount;
			//if (itemCount % rowCount > 0)
			//	rowCount++;
			
			//adjust the height of the container so that it will just barely fit all its children
			//float scrollHeight = height * rowCount;
			//containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
			//containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);
			
			int j = 0;
			for (int i = 0; i < itemCount; i++)
			{
				//this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
				if (i % columnCount == 0)
					j++;
				
				//create a new item, name it, and set the parent
				//GameObject newItem = Instantiate(itemPrefab) as GameObject;
				GameObject newItem=GameObject.Find( gameObject.name + " item at (" + i + "," + j + ")");
				//newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
				//newItem.transform.parent = gameObject.transform;

				newItem.transform.FindChild("Heading").GetComponent<Text> ().text="";
				newItem.transform.FindChild("Body").GetComponent<Text> ().text="";


				string newsTitle=(string)title[i+1];
				string newsDescription=(string)description[i];
				//set the text to the heading and body.
				if(newsTitle.Equals("")){
					newsTitle="Yahoo! Finance: YAHOO News";
				}
				newItem.transform.FindChild("Heading").GetComponent<Text> ().text=newsTitle;
				
				//if(shortStringLength<= title[i+1].ToString().Length){
				//	shortDescription=((string)title[i+1]).Substring(0,shortStringLength )+"...";
				//}else
				//{
				//shortDescription=(string)title[i+1];
				//}
				if(newsDescription.Equals("")){
					newsDescription="Yahoo! Finance: YHOO News  Latest Financial News for Yahoo! Inc.";
				}
				
				newItem.transform.FindChild("Body").GetComponent<Text> ().text=newsDescription;
				
				//move and size the new item
				//RectTransform rectTransform = newItem.GetComponent<RectTransform>();
				
				//float x = -containerRectTransform.rect.width / 2 + width * (i % columnCount);
				//float y = containerRectTransform.rect.height / 2 - height * j;
				//rectTransform.offsetMin = new Vector2(x, y);
				
				//x = rectTransform.offsetMin.x + width;
				//y = rectTransform.offsetMin.y + height;
				//rectTransform.offsetMax = new Vector2(x, y);
			}
				}
		//	ArrayList News= Search("yhoo");	
		
		//change to this format
		/*foreach(ArrayList content in News)
			{
				
				foreach (string line in content)
				{
					//Console.WriteLine(line);
					print (line);
				}
			}*/
		//	ArrayList title=(ArrayList) News[0]; 
		//	ArrayList description=(ArrayList)News[1];
		
		//assuming description length = title length -1
		//	for(int i=1;i<title.Count;i++)
		//{
		//	displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";
		
		//}
		//newsText.transform.GetComponentInChildren<Text> ().text=displayNews;
		//	print (displayNews);
		
		//}
		
		
	}

}