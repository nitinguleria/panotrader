﻿using UnityEngine;
using System.Collections;

public class temp : MonoBehaviour {


	//the code for maps for Microsoft
	public string url = "http://ichart.yahoo.com/b?s=MSFT";
	//http://www.codeproject.com/Articles/37550/Stock-quote-and-chart-from-Yahoo-in-C
	// 1 day: http://ichart.finance.yahoo.com/b?s=MSFT
	// 5 days: http://ichart.finance.yahoo.com/w?s=MSFT
	// 3 months: http://chart.finance.yahoo.com/c/3m/msft
	// 6 months: http://chart.finance.yahoo.com/c/6m/msft
	// 1 year: http://chart.finance.yahoo.com/c/1y/msft
	// 2 years: http://chart.finance.yahoo.com/c/2y/msft
	// 5 years: http://chart.finance.yahoo.com/c/5y/msft
	// Max: http://chart.finance.yahoo.com/c/my/msft
	

//	IEnumerator Start() {
//		WWW www = new WWW(url);
//		yield return www;
//		renderer.material.mainTexture = www.texture;
//		//make the shader transparent.unlit
//	}




	public void days(int d) {

		switch (d)
		{
			case 1:
				StartCoroutine (oneDay ());
				break;
			case 5:
				StartCoroutine (fiveDays ());
				break;
			case 90:
				StartCoroutine (threeMonths());
				break;
			case 180:
				StartCoroutine (sixMonths());
				break;
			case 365:
				StartCoroutine (oneYear());
				break;
			case 1095:
				StartCoroutine (fiveYears ());
				break;
			Debug.Log ("Never should've gotten here");
		}

		//make the shader transparent.unlit
	}



	IEnumerator oneDay() {
		WWW www = new WWW("http://ichart.finance.yahoo.com/b?s=MSFT");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator fiveDays() {
		WWW www = new WWW("http://ichart.finance.yahoo.com/w?s=MSFT");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator threeMonths() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/3m/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator sixMonths() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/6m/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator oneYear() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/1y/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator twoYears() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/2y/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator fiveYears() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/5y/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}
	IEnumerator max() {
		WWW www = new WWW("http://chart.finance.yahoo.com/c/my/msft");
		yield return www;
		renderer.material.mainTexture = www.texture;
		//make the shader transparent.unlit
	}




}
