﻿using UnityEngine;
using System.Collections;
/**
 * Author Arjun Subramanian
 * Create the method 
 * 
 * 
 * **/

using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
/**
 * Companies:
 * ABT,ABBV,ACE,ACN,ACT,ADBE,ADT,AES,AET,AFL,A,GAS,APDARG,AKAM,AA,ALXN,ATIALLE,AGN,ADS,ALL,ALTR,MO,,AMZNAEE,AEP,AXP,AIG,AMT,,AMP,
 * ABC,AME,AMGN,APHAPC,,ADI,AONAPA,,AIVAAPL,AMAT,ADM,AIZ,T,ADSK,ADP,,AN,AZO,AVGO,,AVB,AVY,AVP,BHI,BLL,BAC,BK,BCR,BAX,BBT,BDX,BBBY,
 * BMSBRK.B,BBY,BIIB,BLK,HRB,BA,BWA,BXP,BSX,BMY,BRCM,BF.B,CHRW,CACVC,COG,CAM,CPB,COF,CAH,CFN,KMX,CCL,CAT,CBGCBS,CELG,CNP,CTL,CERN,
 * CF,SCHW,CHK,CVX,CMGCB,CI,,XECCINF,CTAS,CSCO,C,CTXS,CLX,CME,CMS,COH,KOCCE,CTSH,CL,CMCSA,CMA,CSC,CAG,COP,CNX,ED,STZ,GLW,COST,COV,
 * CCI,CSX,CMI,CVS,DHI,DHR,DRI,DVA,DE,DLPH,DAL,DNR,XRAY,DVN,DO,DTV,DFS,DISCA,DG,DLTR,*,,,DOV,DOW,DPS,DTEDD,DUK,DNB,ETFCEMN,ETN,EBAY
 * ,ECL,EIX,EW,EA,EMC,EMR,ESV,ETREOG,EQT,EFX,EQRESS,EL,EXC,EXPE,EXPDESRX,XOM,FFIV,,BFDO,FAST,FDX,FIS,FITB,FSLR,FE,FISV,FLIR,FLS,
 * FLRFMC,FTI,F,FRX,,FOSL,BEN,FCX,FTR,GME,GCI,GPS,GRMN,GD,GE,GGP,GIS,GM,GPC,GNW,GILD,GS,GT,GOOG,GOOGLGWW,HAL,HOG,HAR,HRS,HIG,HAS,
 * HCP,HCN,HP,HES,HPQ,HD,HON,,,RL,HSPST,HCBK,HUM,HBAN,ITW,IR,TEG,,INTCICE,,IBMIP,IPG,,IFF,INTUISRG,IVZ,,IRM,BL,EC,,NJ,JCI,JOY,JPM,
 * ,NPR,,KSU,,KEYGMCR,KMB,KIM,KMI,KLACKSS,KRFT,KR,LB,LLL,LHLRCX,LM,LEG,LEN,LUK,LLY,LNCLLTC,LMT,L,LO,LOW,LYB,MTB,MAC,M,MMM,MRO,MPC,
 * MAR,MMC,MAS,MA,MAT,MKC,MCD,MHFI,MCK,MJN,MWV,MDT,MRK,MET,MCHPMU,MSFT,MHK,TAP,MDLZMON,MNST,MCO,MS,MOS,MSI,MUR,MYL,NBR,NDAQNOV,NAVI
 * ,NTAP,NFLX,NWL,NFX,,NEM,NWSA,NEE,NLSN,NKE,NINE,,NBL,JWN,NSC,NTRS,NOC,NUNRG,NUE,NVDA,KORS,ORLY,OXY,OMC,OKE,ORCL,OI,PCG,,PCAR,PLL,
 * PH,PDCO,PAYX,BTU,PNR,PBCT,POM,PEP,PKIPRGO,PETM,PFE,PM,PSX,PNW,PXD,,PBI,PCL,PNC,RL,PPG,PPL,PX,PCP,PCLN,PFG,PG,PGR,PLD,PRU,PEG,PSA
 * ,PHM,PVH,QEP,PWR,QCOM,DGX,RRC,RTN,RHT,REGN,RF,RSG,RAI,RHI,ROK,COL,ROP,ROST,RDC,R,SWY,CRM,SNDK,SCG,SLB,SNI,STX,SEE,SRE,SHW,SIAL
 * ,SPGSJM,SNA,SO,LUV,SWN,SE,STJ,SWK,SPLS,SBUX,HOTSTT,SRCL,SYK,STI,SYMC,SYY,TROW,TGT,TEL,TE,THC,TDC,TSO,TXN,TXT,HSY,TRV,TMO,TIF,TWX
 * ,TWC,TJX,TMKTSS,TSCO,RIG,TRIP,FOXA,TSN,TYC,USB,UA,UNP,UNH,UPS,X,UTX,UNMURBN,VFC,VLO,VAR,VTR,VRSN,VZ,VRTX,,VIAB,V,VNO,VMC,WMT,WAG
 * ,DIS,GHC,WM,WAT,WLP,WFC,WDC,WU,WY,WHR,WFMWMB,IN,WEC,WYN,WYNN,XEL,XRX,XLNX,XL,XYL,YHOO,YUM,ZMH,ZION,ZTS,, ********/

/** Headers
 *  Name
    Index
 * Price
   Volume
 * Open
 * YearHigh
 * 
   YearLow
  ChangeInPercent
 * 
 * **/
public class SearchFunctionScript : MonoBehaviour {
	private const string BASE_URL = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20({0})&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
	GameObject searchInputField;
	GameObject searchresultText;
	//GameObject newsText;
	 GameObject PrefabForSearchResultsButton;
	 GameObject ColumnButtonPrefab;
	public TextAsset myTextFile ;

	 GameObject ExistingButton; 
	GameObject searchResultButtons;
	string searchinputfieldText="";
	string displayNews="";
	
	ArrayList GlobalIndicesData= new ArrayList();
	GameObject headingPanel;

	//search results 
	public void searchButtonClick(string id){

		// input field
		searchInputField = GameObject.Find ("SearchInputField");
		searchresultText=GameObject.Find("SearchresultText");
		//searchinputfieldText = searchInputField.transform.GetComponentInChildren<Text> ().text;
		//newsText = GameObject.Find ("NewsText");
		if (id.Equals ("searchButton")) {
			ArrayList Codes = new ArrayList ();
			string symbolList = "";
			bool SecondTime = false;
			
			//search code based on input bar and store in array
			searchinputfieldText = searchInputField.transform.GetComponentInChildren<Text> ().text;
			print ("searchinputfieldText " + searchinputfieldText);
			Codes = SearchStock (searchinputfieldText,myTextFile);
			string url = "";
			string[] codeArray = new string[Codes.Count];
			int index = 0;
			foreach (string code in Codes) {
				codeArray [index] = code;
				index++;
				//displayNews+=code+ "\n ";
				//print(code);
			}
			index = 0;
			//newsText.transform.GetComponentInChildren<Text> ().text=displayNews;
			//displayNews="";
			
			//get company name from code
			
			foreach (string line in Codes) {
				if (SecondTime) {
					symbolList += "%2C";
				}
				symbolList += "%22" + line.Trim () + "%22";
				print (symbolList);
				SecondTime = true;
			}
			url = string.Format (BASE_URL, symbolList);
			
			ArrayList Stocks = new ArrayList ();
			Stocks = Fetch (url, codeArray, Stocks);
			
			//set the value of company name and code in the search output
			int counter = 0;
			foreach (ArrayList stock in Stocks) {
				//display only 4 results max
				if(counter>=1)
				{ break;}
				foreach (string field in stock) {
					
					//make a button for each search results
					//GameObject tempButtonObject = (GameObject)Instantiate (PrefabForSearchResultsButton);
					//tempButtonObject.transform.parent = GameObject.Find ("ScreenFinancialNews").transform;
					//tempButtonObject.transform.localPosition = new Vector3(0,0,0);
					//tempButtonObject.transform.rotation = GameObject.Find ("ScreenFinancialNews").transform.rotation.identity;
					
					//tempButtonObject.transform.localPosition = GameObject.Find ("ScreenFinancialNews").transform.position;
					//tempButtonObject.transform.localRotation= GameObject.Find ("ScreenFinancialNews").transform.rotation;
					//tempButtonObject.transform.localScale=GameObject.Find ("ScreenFinancialNews").transform.localScale;
					//searchResultButtons= Instantiate
					searchInputField.transform.GetComponentInChildren<Text> ().text=codeArray[0];
					searchresultText.transform.GetComponentInChildren<Text> ().text=field;
					
					displayNews += " " + field + " ( " + codeArray [counter] + " ) \n";
					counter++;
					break;
				}
				
				
			}
			counter = 0;

			displayNews = "";
		}	
	}

	//retrieve symbol lists in Arraylist Stock, an empty arraylist Stocks is passed and returned with values of stocks,called in StockFeed
	public static ArrayList Fetch(String url,string[] Symbol,ArrayList Stocks)
	{
		//ArrayList Stock = new ArrayList();
		try
		{
			XDocument doc = XDocument.Load(url);
			Parse(Stocks, doc, Symbol);
		}
		catch(Exception e)
		{
			print ("Not working" +url);
		}
		
		
		return Stocks;
	}
	//search function to be added in a common class , need to add company name in brackets like GOOG(google)
	static ArrayList SearchStock(String Name, TextAsset myTextFile1)
	{
		Boolean correctCode = false;
		string[] Companycodes=myTextFile1.text.Split(","[0]);
	
		//var reader = new StreamReader(File.OpenRead(@"sp500Company.csv"));
		List<string> CompanyCode = new List<string>();
		List<string> CompanyName = new List<string>();
		ArrayList codes = new ArrayList();
		int count = Companycodes.Length;
		for(int j =0 ;j<count-1;j++)
		{


			CompanyCode.Add(Companycodes[j+1]);
			CompanyName.Add(Companycodes[j]);
			j++;

		}
		int i;
		
		//check for correct code
		for( i =0;i<CompanyCode.Count();i++)
		{   
			if (CompanyCode[i].Contains(Name.ToUpper()))
			{    correctCode=true;
				print(CompanyCode[i]);
				codes.Add(CompanyCode[i].ToUpper());
				
			}
		}
		//if found return codes
		if (correctCode) {
			return codes;		
		}
		// if not ,check for correct name
		for( i =0;i<CompanyName.Count();i++)
		{   
			if (CompanyName[i].ToUpper().Contains(Name.ToUpper()))
			{print(CompanyCode[i]);
				codes.Add(CompanyCode[i]);
				
			}
		}
		
		return codes;
	}
	//  parse stockdata in Arraylist stocks, this function is used in Fetch 
	private static ArrayList Parse(ArrayList Stocks, XDocument doc,string[] symbols)
	{
		XElement results = doc.Root.Element("results");
		
		foreach (string symbol in symbols)
		{   //Console.WriteLine(symbol);
			//print (symbol);
			ArrayList Stock = new ArrayList();
			if ((results.Elements("quote").First(w => w.Attribute("symbol").Value == symbol.Trim()) != null))
			{
				XElement q = results.Elements("quote").First(w => w.Attribute("symbol").Value == symbol.Trim());
				
				
				String Price = (q.Element("Ask").Value).ToString();
				//add everything to stock
				String AverageDailyVolume = (q.Element("AverageDailyVolume").Value).ToString();
				String Name = (q.Element("Name").Value).ToString();
				String Open = (q.Element("Open").Value).ToString();
				String Index = (q.Element("StockExchange").Value).ToString();
				String Volume = (q.Element("Volume").Value).ToString();
				String YearHigh = (q.Element("YearHigh").Value).ToString();
				String YearLow = (q.Element("YearLow").Value).ToString();
				String ChangeInPercent = (q.Element("ChangeinPercent").Value).ToString();
				Stock.Add(Name);
				Stock.Add(Index);
				Stock.Add(Price);
				Stock.Add(Volume);
				Stock.Add(Open);
				Stock.Add(YearHigh);
				Stock.Add(YearLow);
				Stock.Add(ChangeInPercent);
				//pass everything to Stocks
				Stocks.Add(Stock);
			}
			else
			{
				print (symbol);
			}
		}
		return Stocks;
	}
}
