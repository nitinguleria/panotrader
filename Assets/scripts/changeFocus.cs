﻿using UnityEngine;
using System.Collections;

public class changeFocus : MonoBehaviour {

	public GameObject currentFocusPanel;
	public GameObject camera;
	private static Vector3 prevPositionFocusPanel;
	private static Quaternion prevRotationFocusPanel;
	private static Vector3 prevCameraPosition;
	private static Quaternion prevCameraRotation;
	private static bool attached = false;
	public GameObject PanoMenu;
	public GameObject miniButtons;
	public GameObject buttonsWithinPanel;


	public void detectPanel()
	{
		print ("do you get here?");
		RaycastHit hit;
		int layerMaskPanel = 1 << 8;

		if (attached)
		{
			//return the window to the position it as attached in
			currentFocusPanel.transform.parent = null;
			currentFocusPanel.transform.position = prevPositionFocusPanel;
			currentFocusPanel.transform.rotation = prevRotationFocusPanel;
			currentFocusPanel.gameObject.layer = 8;
			attached = false;
			camera.transform.position = prevCameraPosition;

			//disable PanoMenu
			PanoMenu.GetComponent<Canvas>().enabled = false;
			miniButtons.GetComponent<Canvas>().enabled = false;
			//miniButtons.SetActive(false); wouldn't this be a better solution than above??
			buttonsWithinPanel.GetComponent<buttonsWithinPanel>().hidePrevButtons();
		}
		else if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, 1000F, layerMaskPanel)) // if you found an existing panel
		{	
			//load up the new panel
			Debug.Log ("what did you hit?" + hit.transform.name);
			Vector3 temp= new Vector3();
			temp = hit.point;

			currentFocusPanel = (GameObject)hit.transform.gameObject; //save currently focused panel

			prevPositionFocusPanel = currentFocusPanel.transform.position; //save the positon of currently focused panel beore attaching to the main camera
			prevRotationFocusPanel = currentFocusPanel.transform.rotation; //save the rotation of currenlty focused panel
			Debug.DrawLine (transform.position, hit.point, Color.green, 1000F); //smark debugging purpose
			currentFocusPanel.transform.parent = camera.transform; //attach to camera by making a child of camera

			currentFocusPanel.transform.localPosition = new Vector3(0,0,7); //this is to bring the panel closer to the screen
			currentFocusPanel.transform.localRotation = Quaternion.Euler(0,0,0);
			//currentFocusPanel.transform.rotation = Quaternion.LookRotation(currentFocusPanel.transform.position - look_at_target.transform.position); //make it face the camera
			currentFocusPanel.gameObject.layer = 11;
			attached = true;

			//change the location of camera to some remote place
			prevCameraPosition = camera.transform.position;
			camera.transform.position = new Vector3(100,100,100);

			//load up panoMenu
			PanoMenu.GetComponent<Canvas>().enabled = true;

		}
		print ("do you get here?");
	}
		

}
