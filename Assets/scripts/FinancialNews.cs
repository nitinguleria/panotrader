﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using System.Xml;
using System.Collections;
public class FinancialNews : MonoBehaviour {



	//copy from YahooNewsFeed.cs or Program.cs fron Arjun's Code
	GameObject searchInputField;
	//GameObject newsText;
	string displayNews="";
	//get default yahoo news
	public ArrayList getNews()
	{
		// if you want to make this URL genric you can give a input method which takes user or your input 
		//and assign it to  a variable and replace the hard coded yhoo to the variable 
		
		var URL = "http://finance.yahoo.com/rss/headline?s=yhoo";
		var webClient = new WebClient();
		
		var directory = @"..\..\data\";
		var dayDirectory = directory + @"\Daily_Data\";
		var newFile = Application.persistentDataPath+"News.XML";
		
		webClient.DownloadFile(URL, newFile);
		
		ArrayList News = ReadXML(Application. persistentDataPath+"News.XML");
		return News;
	}

	// click button for search button
	public void SearchButtonClick(string id)
	{  
		searchInputField= GameObject.Find("SearchInputField");
	//	newsText = GameObject.Find ("NewsText");


		if (id.Equals ("searchButton")) {
			ArrayList News= Search("sekey");	

			//change to this format
			/*foreach(ArrayList content in News)
			{
				
				foreach (string line in content)
				{
					//Console.WriteLine(line);
					print (line);
				}
			}*/
			ArrayList title=(ArrayList) News[0]; 
			ArrayList description=(ArrayList)News[1];

			//assuming description length = title length -1
			for(int i=1;i<title.Count;i++)
			{
				displayNews+= " "+ title[i] + "\n   " + description[i-1] +"\n";

			}
			//newsText.transform.GetComponentInChildren<Text> ().text=displayNews;
			print (displayNews);
		    
		}
		}

	ArrayList ReadXML(string fileName)
	{
		ArrayList news = new ArrayList();
		ArrayList title = new ArrayList();
		ArrayList description = new ArrayList();
		using (XmlReader reader = XmlReader.Create(fileName))
		{
			while (reader.Read())
			{
				if (reader.IsStartElement())
				{
					
					switch (reader.Name)
					{
					case "title":
						
						
						
						if (reader.Read())
						{
							string tit = reader.Value.Trim();
							title.Add(tit);
							//print(tit);
						}
						break;
					case "description":
						if (reader.Read())
						{
							
							string desc = reader.Value.Trim();
							description.Add(desc);
							//print(desc);
						}
						
						break;
					}   
					
					
				}
			}
		}
		news.Add(title);
		news.Add(description);
		return news;
	}
	// Takes Search String and returns a set of news based on Search       
	public ArrayList Search(string search)
	{

		var URL = "http://finance.yahoo.com/rss/headline?s=" + search;
		print (URL + "  ::in search function");
		var webClient = new WebClient();
		
		var directory = @"..\..\data\";
		var dayDirectory = directory + @"\Daily_Data\";
		var newFile = Application. persistentDataPath+"NewsSearch.XML";
		
		webClient.DownloadFile(URL, newFile);
		
		ArrayList News = ReadXML(Application. persistentDataPath+"NewsSearch.XML");
		return News;
		
	}


}
