﻿using UnityEngine;
using System.Collections;

public class GraphsAPI : MonoBehaviour {

	GameObject screenGraph; // Graph Screen Panel
	string urlRoot="http://ichart.finance.yahoo.com/"; //Yahoo Chart Api URL
	string duration=oneDay; // Default duration of the Chart Data retrieved 
	WWW www;
	private const string oneDay="b?s=";
	private const string fiveDay="w?s=";
	private const string threeMonth="c/3m/";
	private const string sixMonth="c/6m/";
	private const string oneYear="c/1y/";
	private const string twoYear="c/2y/";
	private const string fiveYear="c/5y/";
	private const string Max ="c/my/";

	void Start () {
		StartCoroutine (getGraphImage ("Input", oneDay));
	}

	IEnumerator getGraphImage(string searchText,string duration) {
		//output image
		screenGraph = GameObject.Find ("ScreenGraph");
		
		//default is microsoft
		if(searchText.Equals("Input"))
		{	
			www = new WWW(urlRoot+ duration+"msft");
		}
		else
		{
			www = new WWW(urlRoot+ duration+searchText);
		}
		
		yield return www;
		
		screenGraph.transform.renderer.material.mainTexture = new Texture2D(128, 128, TextureFormat.RGB24, false);
		www.LoadImageIntoTexture (screenGraph.transform.renderer.material.mainTexture as Texture2D);
		
	}

	// Update is called once per frame
	void Update () {
	
	}
}
